![Build Status](https://gitlab.com/23f-its-projekt/23f-its-project-description/badges/main/pipeline.svg)

# UCL IT Sikkerhed projekt forår 2023

Side til projektbeskrivelse af IT Sikkerheds projektet forår 2023

Link til website: [https://23f-its-projekt.gitlab.io/23f-its-project-description](https://23f-its-projekt.gitlab.io/23f-its-project-description)

Link til projektgruppe: [https://gitlab.com/23f-its-projekt](https://gitlab.com/23f-its-projekt)

# Efter projektet er klonet

1. Lav et virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) windows: `py -3.10 -m venv env`
2. Aktiver virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment) windows: `.\env\Scripts\activate`
3. Installer pip dependencies `pip install -r requirements.txt`
4. Kør siden lokalt fra pages mappen `mkdocs serve`

## Dokumentation

- MKDocs [https://www.mkdocs.org/](https://www.mkdocs.org/)
- Theme [https://github.com/squidfunk/mkdocs-material](https://github.com/squidfunk/mkdocs-material)
- More on Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
- Git revision plugin [https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/](https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/)
- linkchecker [https://github.com/scivision/linkchecker-markdown](https://github.com/scivision/linkchecker-markdown)
- PDF builder [https://github.com/brospars/mkdocs-page-pdf](https://github.com/brospars/mkdocs-page-pdf)
