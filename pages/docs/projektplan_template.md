# Baggrund

*Hvad er baggrunden for projektet*

# Formål

*Hvad er formålet med projektet*

# Mål

*Projektet leverer følgende*

## Nice to have

1. ..
2. ..
3. ..

## Need to have

1. ..
2. ..
3. ..

# Tidsplan

## uge 8
- projektide
- projektplan

## uge 9
## uge ..
## uge 23
- eksamen 19+20 juni

# Organisation

Hvem er medlemmerne i teamet og hvad er deres roller i projektet.  

# Budget og resourcer

Kan udelades hvis ikke aktuelt

# Risikostyring

Hvilke risici er der ifht. projektets fuldførelse og hvordan skal de håndteres hvis de opstår?

Dette punkt kræver en **realistisk** vurdering af hvad der kan gå galt i projektet samet hvilke tiltage teamet beslutter skal udføres hvis den enkelte risici opstår.

Her bør også angives hvilken metode der anvendes til risiko styring. 

Inspiration kan hentes her [https://www.cfl.dk/artikler/risikoanalyse](https://www.cfl.dk/artikler/risikoanalyse)

# Interessenter

Her skrives resultatet af interessentanalyse

Inspiration kan hentes her [https://www.cfl.dk/artikler/interessentanalyse](https://www.cfl.dk/artikler/interessentanalyse)

# kommunikation

Hvordan er det besluttet at kommunikere i projektet og er der ud fra interessant samt risiko analysen behov for at etablere kommunikation der understøtter dem.

## Kommunikationskanaler

- gitlab issue board
- element
- discord
- email adresser
- mm.

## Kommunikationsaktiviteter

- Møder
- Periodiske emails
- Branching strategier
- Kommunikation med eksterne
- mm.

# Perspektiv

Projektet danner grundlag for uddannelsens øvrige semesterprojekter og skal opfattes som en øvelse i at lave et godt bachelor projekt som afslutning på uddannelsen.

# Evaluering

Hvordan evalueres projektet udover de obligatoriske eksamens afleveringer.  
Det anbefales at evaluere alle projekter ved afslutning for at synliggøre og samle erfaringer til brug i næste projekt.  
Det er også en god ide at, som team, at kigge tilbage på projektets forløb for at reflektere over hvad de enekelte teammedlemmer har lært i løbet af projektet.

Endelig bør et godt udført og veldokumenteret projekt inkluderes i en studerendes portfolio/dokumentation side så det er muligt for andre at få glæde af projeket.

# Referencer

Relevante overordnede referencer til essentielle ting der benyttes i projektet, hver reference bør have en beskrivelse der kort begrunder og forklarer hvordan referencer er relevant for projektet.

- Links til dokumentation
- Links til metoder
- mm.
